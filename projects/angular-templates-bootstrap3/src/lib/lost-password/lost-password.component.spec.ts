import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { Bootstrap3LostPasswordComponent } from './lost-password.component'

describe('Bootstrap3LostPasswordComponent', () => {
  let component: Bootstrap3LostPasswordComponent
  let fixture: ComponentFixture<Bootstrap3LostPasswordComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bootstrap3LostPasswordComponent ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(Bootstrap3LostPasswordComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

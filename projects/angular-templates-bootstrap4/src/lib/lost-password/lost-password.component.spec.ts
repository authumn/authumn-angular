import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { Bootstrap4LostPasswordComponent } from './lost-password.component'

describe('Bootstrap4LostPasswordComponent', () => {
  let component: Bootstrap4LostPasswordComponent
  let fixture: ComponentFixture<Bootstrap4LostPasswordComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bootstrap4LostPasswordComponent ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(Bootstrap4LostPasswordComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

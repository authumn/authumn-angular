import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'

import {
  UserComponentsModule,
  UserTemplates
} from '@authumn/angular-user'

import { Bootstrap4LoginComponent } from './login/login.component'
import { Bootstrap4RegisterComponent } from './register/register.component'
import { Bootstrap4SocialComponent } from './social/social.component'
import { Bootstrap4LostPasswordComponent } from './lost-password/lost-password.component'

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    UserComponentsModule
  ],
  declarations: [
    Bootstrap4LoginComponent,
    Bootstrap4RegisterComponent,
    Bootstrap4SocialComponent,
    Bootstrap4LostPasswordComponent
  ],
  exports: [
    Bootstrap4LoginComponent,
    Bootstrap4RegisterComponent,
    Bootstrap4SocialComponent,
    Bootstrap4LostPasswordComponent
  ],
  providers: [
    {
      provide: UserTemplates,
      useValue: {
        formFramework: 'bootstrap-4',
        login: Bootstrap4LoginComponent,
        register: Bootstrap4RegisterComponent,
        social: Bootstrap4RegisterComponent,
        lostPassword: Bootstrap4LostPasswordComponent
      }
    }
  ]
})
export class Bootstrap4TemplatesModule {}
// provide configuration with yet another forRoot() method?

import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'

import {
  UserComponentsModule,
  UserTemplates
} from '@authumn/angular-user'

import { MaterialLoginComponent } from './login/login.component'
import { MaterialRegisterComponent } from './register/register.component'
import { MaterialTabsComponent } from './tabs/tabs.component'
import { MaterialSocialComponent } from './social/social.component'
import { MaterialModule } from './material.module'
import { MaterialLostPasswordComponent } from './lost-password/lost-password.component'

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    UserComponentsModule
  ],
  declarations: [
    MaterialLoginComponent,
    MaterialRegisterComponent,
    MaterialTabsComponent,
    MaterialSocialComponent,
    MaterialLostPasswordComponent
  ],
  exports: [
    MaterialLoginComponent,
    MaterialRegisterComponent,
    MaterialTabsComponent,
    MaterialSocialComponent,
    MaterialLostPasswordComponent
  ],
  providers: [
    {
      provide: UserTemplates,
      useValue: {
        formFramework: 'material-design',
        register: MaterialRegisterComponent,
        login: MaterialLoginComponent,
        lostPassword: MaterialLostPasswordComponent
      }
    }
  ]
})
export class MaterialTemplatesModule {}

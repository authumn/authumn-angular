import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { MaterialLostPasswordComponent } from './lost-password.component'

describe('MaterialLostPasswordComponent', () => {
  let component: MaterialLostPasswordComponent
  let fixture: ComponentFixture<MaterialLostPasswordComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialLostPasswordComponent ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialLostPasswordComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

#!/usr/bin/env bash
env="production"
rm -rf dist/
ng build @authumn/angular-auth --configuration "$env"
ng build @authumn/angular-user --configuration "$env"
ng build @authumn/angular-templates-bootstrap3 --configuration "$env"
ng build @authumn/angular-templates-bootstrap4 --configuration "$env"
ng build @authumn/angular-templates-material --configuration "$env"
ng build @authumn/angular --configuration "$env"

